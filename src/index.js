const chaiGraphql = require('chai-graphql');

/*
  A graphql connection must:
  - have required properties
    - edges
      - cursor
      - node
    - count
    - pageInfo
      - hasPreviousPage
      - hasNextPage
 */
module.exports = (chai) => {
  chai.use(chaiGraphql);

  const { Assertion } = chai;

  Assertion.addProperty('relayConnection', function assertIsRelayConnection() {
    const obj = this._obj; // eslint-disable-line no-underscore-dangle

    this.assert(
      Array.isArray(obj.edges),
      'expected #{this}.edges to be an array of edges',
      'expected #{this}.edges to not be an array of edges'
    );

    this.assert(
      obj.edges.every((e) => e.cursor && e.node),
      'every edge must have properties cursor and node'
    );

    this.assert(
      Number.isInteger(obj.count),
      'expected #{this}.count to be an integer',
      'expected #{this}.count to not be an integer'
    );

    this.assert(
      obj.pageInfo !== undefined
        && obj.pageInfo.hasNextPage !== undefined
        && obj.pageInfo.hasPreviousPage !== undefined,
      'expected #{this}.pageInfo to be an object with properties hasNextPage and hasPreviousPage',
      'expected #{this}.pageInfo to not be an object with properties hasNextPage and hasPreviousPage'
    );
  });
};
