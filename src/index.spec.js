/* eslint-disable no-unused-expressions */
const { describe, it } = require('mocha');
const chai = require('chai');

const { assert, expect } = chai;
const chaiGraphql = require('./index');

chai.use(chaiGraphql);

const validConnection = {
  edges: [{ cursor: 'string', node: {} }],
  count: 9,
  pageInfo: {
    hasPreviousPage: true,
    hasNextPage: true
  }
};

describe('chaiGraphql', () => {
  describe('.relayConnection', () => {
    it('have required properties of a relay connection', () => {
      const invalidConnection = {
        ...validConnection,
        count: 'not-a-number'
      };

      expect(validConnection).to.be.a.relayConnection;
      assert.throws(() => expect(invalidConnection).to.be.a.relayConnection);
    });
  });
});
