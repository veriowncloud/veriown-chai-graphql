# @veriown/chai-graphql

> Chai plugin for assisting writing chai assertions for GraphQL related code

This package extends [chai-graphql](https://github.com/bustle/chai-graphql), so all the assertions
supported by chai-graphql are supported as well. Following extra assertions are provided

## relayConnection

Checks if an object is a valid relay-connection. A graphql connection must have required properties:

- edges
    - cursor
    - node
- count
- pageInfo
    - hasPreviousPage
    - hasNextPage

e.g

```
expect(anObject).to.be.a.relayConnection;
```
